<?php
#Name:Capitalize First Word of Each Sentence v1
#Description:Capitalize the first letter of the start of each sentence (eg: 'this is a sentence' becomes 'This is a sentence'). Returns text as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'string' is a string containing the text to increment. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('capitalize_first_word_of_each_sentence_v1') === FALSE){
function capitalize_first_word_of_each_sentence_v1($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($string) === FALSE){
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Capitalize the first character of the first word.
		$string = @ucfirst($string);
		###Capitalize the first character of every sentence, after the first one.
		$string = preg_replace_callback(
			'/[.!?] [a-z]/',
			function ($matches){
				$return = @strtoupper($matches[0]);
				return $return;
			}, $string
		);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('capitalize_first_word_of_each_sentence_v1_format_error') === FALSE){
				function capitalize_first_word_of_each_sentence_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("capitalize_first_word_of_each_sentence_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $string;
	} else {
		return FALSE;
	}
}
}
?>